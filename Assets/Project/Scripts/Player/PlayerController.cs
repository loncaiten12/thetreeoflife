﻿using Sirenix.OdinInspector;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField]
    private float speed, sen, baseGravity;
    [SerializeField]
    private CharacterController characterController;
    [SerializeField]
    private Camera camera;
    [SerializeField]
    private LayerMask enemyMask;
    [SerializeField]
    private LayerMask groundMask;
    [SerializeField]
    private Weapon currentWeapon;
    [SerializeField]
    private EnemyEvent enemyEvent;
    [SerializeField]
    private Vector3 craftPosition;
    [SerializeField]
    private bool isCrafting;
    [SerializeField]
    private bool canCraft;
    [SerializeField]
    private GameObject previewTurret;
    [SerializeField]
    private float craftDistance;

    private Vector3 previewTurretPosition;
    private float xRotation, yRotation;
    private Vector3 moveDirection = Vector3.zero;
    private Vector2 move = new Vector2();
    private Vector2 rotate;

    private void Awake()
    {
        previewTurret.SetActive(false);
        previewTurret.transform.position = previewTurretPosition;
        previewTurretPosition.z = craftDistance;
    }

    private void Update()
    {
        Move(move);
        Rotate(rotate);
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            Shoot();
        }
        if (isCrafting)
        {

            CraftDetect();
        }
    }

    public void StartCrafting()
    {
        previewTurret.SetActive(true);
        isCrafting = true;
    }
    public void Crafted()
    {
        previewTurret.SetActive(false);
        isCrafting = false;
    }
    public void CraftDetect()
    {
        RaycastHit hit;
        if (Physics.Raycast(camera.transform.position, camera.transform.TransformDirection(Vector3.forward), out hit, craftDistance, groundMask))
        {
            previewTurret.transform.GetComponent<Renderer>().material.color = Color.green;
            craftPosition = hit.point;
            canCraft = true;
        }
        else
        {
            craftPosition = Vector3.zero;
            previewTurret.transform.GetComponent<Renderer>().material.color = Color.red;
            canCraft = false;
        }
    }

    public void Craft(Turret turret)
    {
        if (craftPosition != Vector3.zero)
        {
            Instantiate(turret, craftPosition, Quaternion.identity);
        }
    }

    [Button]
    public void Shoot()
    {
        RaycastHit hit;
        if (Physics.Raycast(camera.transform.position, camera.transform.TransformDirection(Vector3.forward), out hit, 1000, enemyMask))
        {
            enemyEvent?.Invoke(hit.transform.GetComponent<Enemy>());
        }
    }
    public void Move(Vector2 move)
    {
        moveDirection = new Vector3(move.x, 0, move.y);
        moveDirection = transform.TransformDirection(moveDirection);
        moveDirection *= speed * Time.deltaTime;
        moveDirection.y -= baseGravity * Time.deltaTime;
        characterController.Move(moveDirection);
    }


    public void Rotate(Vector2 rotate)
    {
        float mouseX = rotate.x * sen * Time.deltaTime;
        float mouseY = rotate.y * sen * Time.deltaTime;
        xRotation -= mouseY;
        xRotation = Mathf.Clamp(xRotation, -90f, 90f);
        camera.transform.localRotation = Quaternion.Euler(xRotation, yRotation, 0f);
        transform.Rotate(Vector3.up * mouseX);
    }
}
