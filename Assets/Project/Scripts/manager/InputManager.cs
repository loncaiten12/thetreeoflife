﻿using UnityEngine;

public class InputManager : MonoBehaviour
{
    [SerializeField]
    private FixedTouchField rotatePanel;
    [SerializeField]
    private FixedJoystick moveJoy;
    [SerializeField]
    private Vector2Event moveVector;
    [SerializeField]
    private Vector2Event rotateVector;

    private void Update()
    {
        if (rotatePanel.Pressed)
        {
            rotateVector?.Invoke(rotatePanel.TouchDist);
        }
        if (Mathf.Abs(moveJoy.Horizontal) >= 0.1f || Mathf.Abs(moveJoy.Vertical) >= 0.1f)
        {
            moveVector?.Invoke(moveJoy.Direction);
        }
    }
}
