﻿using Sirenix.OdinInspector;
using System.Collections;
using UnityEngine;
using Random = UnityEngine.Random;

public class WareManger : MonoBehaviour
{
    [SerializeField]
    private WaresConfig gameData;
    [SerializeField]
    private Transform[] spawnPoints;
    [SerializeField]
    private TheTree target;
    [ShowInInspector]
    [ReadOnly]
    private int currentWare = 0;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            StartWare();
        }
    }
    [Button]
    public void StartWare()
    {
        WareConfig ware = gameData.wareConfigs[currentWare];
        for (int j = 0; j < ware.numbersOfEnemy; j++)
        {
            StartCoroutine(SpawnEnemy(ware.enemyPrefab, ware.delaySpawn));
        }
        if (currentWare < gameData.wareConfigs.Length - 1)
        {
            currentWare++;
        }
        else
        {
            currentWare = 0;
        }


    }
    IEnumerator SpawnEnemy(Enemy enemyPrefab, float delay)
    {
        yield return new WaitForSeconds(delay);
        Enemy enemy = Instantiate(enemyPrefab, spawnPoints[(int)Random.Range(0, spawnPoints.Length - 1)].position,
           Quaternion.identity, null);
        enemy.target = this.target;
    }
}
