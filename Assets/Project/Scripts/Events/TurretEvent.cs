﻿using UnityEngine.Events;

[System.Serializable]
public class TurretEvent : UnityEvent<Turret>
{
}
