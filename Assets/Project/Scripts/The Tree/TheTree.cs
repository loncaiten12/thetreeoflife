﻿using UnityEngine;

public class TheTree : MonoBehaviour
{
    [SerializeField]
    private float hp;
    [SerializeField]
    private float rotateSpeed;
    private void Update()
    {
        transform.Rotate(new Vector3(0, rotateSpeed, 0));
    }

    public void TakeHit(float damage)
    {
        hp -= damage;
    }
}
