﻿using UnityEngine;

public class Weapon : MonoBehaviour
{
    [SerializeField]
    private WeaponInfo weaponInfo;
    protected int numberOfArmo;
    protected float lastShoot;
    private void Start()
    {
        Init();
    }
    private void Init()
    {
        numberOfArmo = weaponInfo.maxArmo;
    }
    public void Shoot(Enemy enemy)
    {
        if (numberOfArmo >= 0)
        {
            if (Time.time - lastShoot >= weaponInfo.shootSpeed)
            {
                enemy.TakeDamge(weaponInfo.damage, weaponInfo.addtribute, weaponInfo.effectDuration);
            }
            else
            {
                Debug.Log("Delay");
            }
        }
        else
        {
            Debug.Log("Het dan !");
        }
    }
}
