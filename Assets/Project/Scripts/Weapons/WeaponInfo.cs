﻿using UnityEngine;

[CreateAssetMenu(menuName = "Giang/Weapon Info")]
public class WeaponInfo : ScriptableObject
{
    public int maxArmo;
    public Addtribute addtribute;
    public float shootSpeed;
    public float damage;
    public float effectDuration;
}
