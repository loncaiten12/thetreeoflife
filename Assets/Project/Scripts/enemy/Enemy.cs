﻿using System.Collections;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : MonoBehaviour
{
    [SerializeField]
    protected NavMeshAgent agent;
    [SerializeField]
    protected Animator animator;
    [SerializeField]
    protected float hp;
    [SerializeField]
    protected Collider collider;
    [SerializeField]
    protected float damage;
    [HideInInspector]
    public TheTree target;
    private WaitForSeconds wait = new WaitForSeconds(0.5f);

    // Update is called once per frame
    [System.Obsolete]
    void Update()
    {
        if (Vector3.Distance(transform.position, target.transform.position) < 3f)
        {
            SetDestination(Vector3.zero);
        }
        else
        {
            SetDestination(target.transform.position);
        }
    }


    private void SetDestination(Vector3 targetPosition)
    {
        if (targetPosition != Vector3.zero)
        {
            agent.SetDestination(targetPosition);
            animator.SetTrigger("Walk");
        }
        else
        {
            agent.Stop();
            Hit(target.transform);
        }
    }
    public virtual void TakeDamge(float dmg, Addtribute addtribute, float duration)
    {
        hp -= dmg;
        animator.SetTrigger("GetHit");
        if (hp <= 0)
        {
            Dead();
        }
    }

    public virtual void Dead()
    {
        StartCoroutine(Destroy());
    }

    public virtual void Hit(Transform transform)
    {
        animator.SetTrigger("Hit");
        target.TakeHit(damage);
    }

    IEnumerator Destroy()
    {
        agent.Stop();
        animator.SetTrigger("Die");
        collider.enabled = false;
        yield return wait;
        animator.StopPlayback();
        Destroy(gameObject);
    }
}
