﻿using System.Collections;
using UnityEngine;

public class Spider : Enemy, IStunable
{
    [SerializeField]
    private int numberOfLittleSpider;
    [SerializeField]
    private Transform[] spawnSpiderPoints;
    [SerializeField]
    private LittleSpider littleSpider;
    public override void Dead()
    {
        SpawnLittleSpider();
        base.Dead();
    }

    public override void TakeDamge(float dmg, Addtribute addtribute, float duration)
    {
        base.TakeDamge(dmg, addtribute, duration);
        if (addtribute.Equals(Addtribute.Electric))
        {
            Stun(duration);
        }
    }

    public IEnumerator Stun(float duration)
    {
        float tempSpeed = agent.speed;
        agent.speed = 0;
        yield return new WaitForSeconds(duration);
        agent.speed = tempSpeed;
    }

    private void SpawnLittleSpider()
    {
        for (int i = 0; i < numberOfLittleSpider; i++)
        {
            LittleSpider spider = Instantiate(littleSpider, spawnSpiderPoints[i].position, Quaternion.identity);
            spider.target = this.target;
        }
    }
}
