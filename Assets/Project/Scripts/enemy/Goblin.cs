﻿using System.Collections;
using UnityEngine;

public class Goblin : Enemy, ISlowable
{
    public override void TakeDamge(float dmg, Addtribute addtribute, float duration)
    {
        base.TakeDamge(dmg, addtribute, duration);
        if (addtribute.Equals(Addtribute.Ice))
        {
            StartCoroutine(Slow(duration));
        }
    }

    public IEnumerator Slow(float duration)
    {
        agent.speed *= 0.5f;
        yield return new WaitForSeconds(duration);
        agent.speed /= 0.5f;
    }
}
