﻿using System.Collections;

public interface ISlowable
{
    IEnumerator Slow(float duration);
}
