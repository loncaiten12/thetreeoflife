﻿using System.Collections;

public interface IStunable
{
    IEnumerator Stun(float duration);
}
