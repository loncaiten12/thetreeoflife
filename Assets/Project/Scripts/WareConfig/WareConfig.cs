﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Giang/WareConfig")]
public class WareConfig : ScriptableObject
{
    public int numbersOfEnemy;
    public Enemy enemyPrefab;
    public float delaySpawn;
}
