﻿using UnityEngine;

public class Turret : MonoBehaviour
{
    [SerializeField]
    private TurretInfo turretInfo;
    [SerializeField]
    private LayerMask EnemyLayerMask;
    [SerializeField]
    private Transform topModel;

    private float lastShoot;
    private Enemy target;
    private Collider[] colliders;
    private void Update()
    {
        colliders = Physics.OverlapSphere(transform.position, turretInfo.radius, EnemyLayerMask);
        if (colliders.Length > 0)
        {
            if (Time.time - lastShoot >= turretInfo.shootSpeed)
            {
                target = colliders[0].transform.GetComponent<Enemy>();
                topModel.LookAt(target.transform);
                Shoot();
            }
        }
    }

    private void Shoot()
    {
        target.TakeDamge(turretInfo.damage, turretInfo.addtribute, turretInfo.effectDuration);
    }


}
