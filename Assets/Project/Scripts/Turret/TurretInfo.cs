﻿using UnityEngine;

public class TurretInfo : ScriptableObject
{
    public float hp;
    public float damage;
    public Addtribute addtribute;
    public float effectDuration;
    public Turret turret;
    public float radius;
    public float shootSpeed;
}
