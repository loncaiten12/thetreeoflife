﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;


    /// <summary>
    /// class for test funtion
    /// </summary>
    public static class Utilities
    {
        [MenuItem("Tools/Giang Tool/Clear Editor Preferences")]
        public static void ClearEditorPrefs()
        {
            EditorPrefs.DeleteAll();
        }
    }
