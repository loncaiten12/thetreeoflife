﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Sirenix.OdinInspector.Editor;
using Unity.EditorCoroutines.Editor;
using Sirenix.OdinInspector;
using System;
using System.Linq;
using System.Text;
/// <summary>
/// tool chụp màn hình trên editor
/// </summary>
public class Screenshoter : OdinEditorWindow
    {
        /// <summary>
        /// hiển thị cửa sổ Screenshoter
        /// </summary>
        [MenuItem("Tools/Giang Tool/Screenshoter")]
        private static void ShowWindow()
        {
            Init();
        }
        /// <summary>
        /// khởi tạo
        /// </summary>
        static void Init()
        {
            Screenshoter screenshoter = new Screenshoter();
            GetWindow<Screenshoter>("Giang Editor");
            screenshoter.CamerasOnEditor = screenshoter.GetAllCameraToCamerasOnEditor();
            screenshoter.GetDatas();
            if (screenshoter.screenshotHeight > screenshoter.screenshotWidth)
            {
                screenshoter.Portrait = true;
            }
        }
        // Private Variable
        private bool isCustom => (this.resolution == 6);
        private List<Camera> ScreenCaptureCameras = new List<Camera>();
        private readonly IEnumerable resolutions = new ValueDropdownList<int> {
            { "480P", 1 },
            { "720P", 2 },
            { "1080P", 3 },
            { "2k", 4 },
            { "4k", 5 },
            { "Custom", 6 }
        };
        [Title("Resolution")]
        [OnValueChanged("CheckIsPortrait")]
        [EnableIf("isCustom")]
        [ShowInInspector]
        private int screenshotWidth;

        [OnValueChanged("CheckIsPortrait")]
        [EnableIf("isCustom")]
        [ShowInInspector]
        private int screenshotHeight;

        [ValueDropdown("resolutions")]
        [OnValueChanged("SelectResolution")]
        [ShowInInspector]
        private int resolution;

        [OnValueChanged("RevertWidthAndHeight")]
        [ShowInInspector]
        private bool Portrait;

        [Title("Location")]
        [BoxGroup("Save Location")]
        [ReadOnly]
        [ShowInInspector]
        private string saveLocation;

        [Title("Dict Camera")]
        [ShowInInspector]
        private Dictionary<string, CameraOnEditor> CamerasOnEditor = new Dictionary<string, CameraOnEditor>();

        private bool IsPlay => Application.isPlaying;

        /// <summary>
        /// mở thư mục lưu ảnh gần nhất
        /// </summary>
        [BoxGroup("Save Location")]
        [Button(ButtonSizes.Small)]
        private void OpenLastFolderLoaction()
        {
            Application.OpenURL(this.saveLocation);
        }

        /// <summary>
        /// chụp màn hình với camera tùy chọn
        /// </summary>
        [Button(ButtonSizes.Gigantic)]
        [GUIColor(0.2862745098f, 1f, 0.1725490196f)]
        private void ScreenCapture()
        {
            this.AddCamera();
            string path = this.GetFilePath(this.ScreenCaptureCameras);

            if (!string.IsNullOrEmpty(path))
            {
                this.RenderImage(this.ScreenCaptureCameras, path);
            }
            else
            {
                this.ScreenCaptureCameras.Clear();
            }
        }

        /// <summary>
        /// chụp toàn màn hình game
        /// </summary>
        [EnableIf("IsPlay", true)]
        [Button(ButtonSizes.Gigantic)]
        private void Screenshot()
        {
            string path = this.GetFilePath(this.ScreenCaptureCameras);
            if (!string.IsNullOrEmpty(path))
            {
                path = path + "_All_Cam" + ".png";
                UnityEngine.ScreenCapture.CaptureScreenshot(path);
                EditorCoroutineUtility.StartCoroutine(OpenFile(path),this);
                this.SaveLastSaveLocation(path);
            }
        }
          private IEnumerator OpenFile(string path)
          {
            yield return new WaitForSeconds(1f);
            Application.OpenURL(path);
          }

        /// <summary>
        /// render ảnh!
        /// </summary>
        /// <param name="screenshotWidth">chiều rộng ảnh đầu ra</param>
        /// <param name="screenshotHeight">chiều cao ảnh đầu ra</param>
        /// <param name="Cameras">danh sách camera muốn chụp</param>
        /// <param name="path">đường dẫn lưu ảnh</param>
        private void RenderImage(List<Camera> Cameras, string path)
        {
            int aCameraLength = Cameras.Count;
            // gán đuôi file
            path = path + ".png";
            // save file to disk
            List<Color> listScreenShot = new List<Color>();
            Color[][] srcArray = this.SplitImageToColor(ref Cameras);
            Texture2D outputScreenShot = this.MergeImage(ref aCameraLength, srcArray);
            this.ExportImage(outputScreenShot, path);

            // xóa Progress bar
            EditorUtility.ClearProgressBar();

            // dọn dẹp list CaptureCam 
            this.ScreenCaptureCameras.Clear();

            // lưu đường dẫn
            this.SaveLastSaveLocation(path);
        }

        /// <summary>
        /// phân tách hình ảnh của từng cam thành dạng màu để xử lý
        /// </summary>
        /// <param name="aCamera"></param>
        /// <returns>mang 2 chiều chứa dữ liệu màu của từng cam</returns>
        private Color[][] SplitImageToColor(ref List<Camera> aCamera)
        {
            Color[][] sourcePixels = new Color[aCamera.Count][];
            for (int i = 0; i < aCamera.Count; i++)
            {
                Texture2D screenShot = new Texture2D(this.screenshotWidth, this.screenshotHeight, TextureFormat.ARGB32, false, true);
                // lọc đối tượng theo màu
                RenderTexture renderTexture = new RenderTexture(this.screenshotWidth, this.screenshotHeight, 24)
                {
                    format = RenderTextureFormat.ARGB32,
                    useDynamicScale = true
                };
                RenderTexture.active = renderTexture;
                aCamera[i].targetTexture = renderTexture;
                aCamera[i].Render();
                // chụp
                screenShot.ReadPixels(new Rect(0, 0, this.screenshotWidth, this.screenshotHeight), 0, 0);
                // tách ảnh thành mảng màu add vào mảng 
                sourcePixels[i] = screenShot.GetPixels();
                // setup lại render target
                aCamera[i].targetTexture = null;
                RenderTexture.active = null;
                DestroyImmediate(renderTexture);
            }
            return sourcePixels;
        }

        /// <summary>
        /// chồng ảnh 
        /// </summary>
        /// <param name="outputScreenShot"></param>
        /// <param name="ArrayCamera"></param>
        /// <param name="srcArray"></param>
        /// <returns></returns>
        private Texture2D MergeImage(ref int aCameraLength, Color[][] srcArray)
        {
            Texture2D outputScreenShot = new Texture2D(this.screenshotWidth, this.screenshotHeight, TextureFormat.ARGB32, false, true);
            Color[] colorArray = new Color[this.screenshotHeight * this.screenshotWidth];
            for (int width = 0; width < outputScreenShot.width; width++)
            {
                EditorUtility.DisplayProgressBar("Đang render đợi xiu nha <3", "", (float)((float)width / (float)outputScreenShot.width));
                for (int height = 0; height < outputScreenShot.height; height++)
                {
                    int pixelIndex = width + (height * outputScreenShot.width);
                    for (int g = 0; g < aCameraLength; g++)
                    {
                        Color srcPixel = srcArray[g][pixelIndex];
                        if (srcPixel.a > 0)
                        {
                            colorArray[pixelIndex] = srcPixel;
                        }
                        else if (srcPixel.a > 0 && srcPixel.a < 1)
                        {
                            colorArray[pixelIndex] *= srcPixel;
                        }
                    }
                }
            }
            outputScreenShot.SetPixels(colorArray);
            outputScreenShot.Apply();
            return outputScreenShot;
        }

        /// <summary>
        /// Xuất file ảnh
        /// </summary>
        /// <param name="outputScreenShot"></param>
        /// <param name="saveLocation"></param>
        private void ExportImage(Texture2D outputScreenShot, string saveLocation)
        {
            byte[] bytes = outputScreenShot.EncodeToPNG();
            System.IO.File.WriteAllBytes(saveLocation, bytes);
            Application.OpenURL(saveLocation);
        }

        /// <summary>
        /// chuyển mảng camera sang dạng Dictionary
        /// </summary>
        /// <returns></returns>
        private Dictionary<string, CameraOnEditor> GetAllCameraToCamerasOnEditor()
        {
            Camera[] allCamerasOfGame = Camera.allCameras;
            Dictionary<string, CameraOnEditor> CameraOnEditor = new Dictionary<string, CameraOnEditor>(allCamerasOfGame.Length);

            for (int i = 0; i < allCamerasOfGame.Length; i++)
            {
                CameraOnEditor tempCam = new CameraOnEditor(false, allCamerasOfGame[i], allCamerasOfGame[i].name);
                string key = tempCam.CameraName;
                CameraOnEditor.Add(key, tempCam);
            }

            return CameraOnEditor;
        }

        /// <summary>
        /// cập nhật thông tin camera trong scene
        /// </summary>
        /// <param name="CameraOnEditorUpdate"></param>
        private void UpdateDictCamOnEditor(Dictionary<string, CameraOnEditor> CameraOnEditorUpdate)
        {
            if (this.CamerasOnEditor != CameraOnEditorUpdate)
            {
                Dictionary<string, CameraOnEditor> Intersect =
                this.CamerasOnEditor.Intersect(CameraOnEditorUpdate).ToDictionary(x => x.Key, x => x.Value);
                for (int i = this.CamerasOnEditor.Count - 1; i > 0; i--)
                {
                    string key = this.CamerasOnEditor.ElementAt(i).Key;
                    if (!Intersect.ContainsKey(key))
                    {
                        this.CamerasOnEditor.Remove(key);
                    }
                }
                foreach (KeyValuePair<string, CameraOnEditor> item in CameraOnEditorUpdate)
                {
                    if (!this.CamerasOnEditor.ContainsKey(item.Key))
                    {
                        this.CamerasOnEditor.Add(item.Key, item.Value);
                    }
                }
            }
        }

        /// <summary>
        /// lấy đường dẫn ảnh
        /// </summary>
        /// <param name="listCamera"></param>
        /// <returns></returns>
        private string GetFilePath(List<Camera> listCamera)
        {
            // đặt đường dẫn và tên ảnh 
            StringBuilder fileName = new StringBuilder($"Screenshot_{DateTime.Now.ToString("HH_mm_ss_tt")}");
            if (listCamera.Count > 0)
            {
                // đặt đường dẫn và tên ảnh 
                foreach (Camera cam in listCamera)
                {
                    fileName.Append($"_{cam.name}");
                }
            }
            // mở của sổ lưu file
            return EditorUtility.SaveFilePanel("Chon duong dan luu anh", this.saveLocation, fileName.ToString(), string.Empty);
        }

        /// <summary>
        /// thêm những cam được chọn vào list cam
        /// </summary>
        private void AddCamera()
        {
            foreach (KeyValuePair<string, CameraOnEditor> key in this.CamerasOnEditor)
            {
                if (key.Value.Select)
                {
                    this.ScreenCaptureCameras.Add(key.Value.Camera);
                }
            }
        }

        private void SaveDatas()
        {
            EditorPrefs.SetInt("lastWidth", this.screenshotWidth);
            EditorPrefs.SetInt("lastHeight", this.screenshotHeight);
            EditorPrefs.SetInt("lastResolution", this.resolution);
            EditorPrefs.SetString("lastSaveLocation", this.saveLocation);
        }

        private void GetDatas()
        {
            this.screenshotWidth = EditorPrefs.GetInt("lastWidth");
            this.screenshotHeight = EditorPrefs.GetInt("lastHeight");
            this.resolution = EditorPrefs.GetInt("lastResolution");
            this.saveLocation = EditorPrefs.GetString("lastSaveLocation");
            if (string.IsNullOrEmpty(this.saveLocation))
            {
                this.saveLocation = System.IO.Directory.GetCurrentDirectory();
            }
        }

        /// <summary>
        /// lưu đường dẫn ảnh
        /// </summary>
        /// <param name="path"></param>
        private void SaveLastSaveLocation(string path)
        {
            path = path.Substring(0, path.LastIndexOf("/"));
            this.saveLocation = path;
            EditorPrefs.SetString("lastSaveLocation", path);
        }

        private void SelectResolution()
        {
            switch (this.resolution)
            {
                case 1:
                    this.screenshotWidth = 640;
                    this.screenshotHeight = 480;
                    break;
                case 2:
                    this.screenshotWidth = 1280;
                    this.screenshotHeight = 720;
                    break;
                case 3:
                    this.screenshotWidth = 1920;
                    this.screenshotHeight = 1080;
                    break;
                case 4:
                    this.screenshotWidth = 2560;
                    this.screenshotHeight = 1440;
                    break;
                case 5:
                    this.screenshotWidth = 4096;
                    this.screenshotHeight = 2160;
                    break;
                default:
                    this.resolution = 6;
                    return;
            }
            if (this.Portrait)
            {
                this.RevertWidthAndHeight();
            }
        }

        private void RevertWidthAndHeight()
        {
            int tempValue = this.screenshotWidth;
            this.screenshotWidth = this.screenshotHeight;
            this.screenshotHeight = tempValue;
        }

        /// <summary>
        /// check Portrait when screenshotHeight and  screenshotWidth changing
        /// </summary>
        private void CheckIsPortrait()
        {
            if ((this.screenshotHeight > this.screenshotWidth) != this.Portrait)
            {
                this.Portrait = !this.Portrait;
            }
        }

        private void OnDisable()
        {
            this.SaveDatas();
        }

        private void OnHierarchyChange()
        {
            // lấy danh sách cam mới sau khi Hierarchy thay đổi
            Dictionary<string, CameraOnEditor> CamerasOnEditorUpdate = this.GetAllCameraToCamerasOnEditor();
            this.UpdateDictCamOnEditor(CamerasOnEditorUpdate);
        }

    }




