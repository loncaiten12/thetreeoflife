﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Sirenix.OdinInspector.Editor;
using Sirenix.Utilities.Editor;
using Sirenix.OdinInspector;
using System;

    public struct CameraOnEditor 
    {
    [HideInInspector]
    public Camera Camera;
    [HideInInspector]
    public string CameraName;

    [HorizontalGroup("Cam")]
    [ToggleLeft]
    public bool Select;

    public CameraOnEditor( bool isSelect, Camera camera, string cameraName)
    {
        this.Select = isSelect;
        this.Camera = camera;
        this.CameraName = camera.name;
    }
}